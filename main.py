from subprocess import Popen, run, PIPE, STDOUT, TimeoutExpired
from threading import Thread
from itertools import cycle
from io import BytesIO
import re

from bs4 import BeautifulSoup
import customtkinter as ctk
from PIL import Image
from httpx import get


class Radio(ctk.CTkFrame):
    def __init__(self, master, name):
        super().__init__(master)
        self.columnconfigure(1, weight=1)

        self.master = master
        self.streams = []
        self.name = name
        self.signal = ''
        self.title = ''

        self.get_info()

        image = ctk.CTkLabel(self, text='', image=self.get_image())
        image.bind('<Button-1>', self.mouse)
        image.grid(row=0, column=0, sticky=ctk.NS)

        self.title_label = ctk.CTkLabel(self, text=' ' + self.title)
        self.title_label.grid(row=0, column=1, sticky=ctk.NS + ctk.W)

        self.type_label = ctk.CTkLabel(self, text=self.signal + '  ')
        self.type_label.grid(row=0, column=2, sticky=ctk.NS)

    def mouse(self, _):
        self.master.stream(self)

    def start(self, _type):
        self.title_label.configure(text=f' {self.title} (playing)')
        self.type_label.configure(text=f'({_type}) {self.signal} ')

    def stop(self):
        self.title_label.configure(text=f' {self.title}')
        self.type_label.configure(text=f'{self.signal} ')

    def get_image(self):
        url = f'https://cdn.webrad.io/images/logos/radio-org-nz/{self.name}.png'
        image = Image.open(BytesIO(get(url).content))
        return ctk.CTkImage(light_image=image, dark_image=image, size=(96, 66))

    def get_info(self):
        url = f'https://api.webrad.io/data/streams/71/{self.name}'
        json = get(url, headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.3'
        }).json()['result']

        self.signal = json['station']['signal']
        self.title = json['station']['title']

        streams = []
        for stream in json['streams']:
            streams.append({'type': stream['mediaType'], 'url': stream['url']})
        self.streams = cycle(streams)


class Main(ctk.CTk):
    def __init__(self):
        super().__init__()
        self.resizable(False, False)
        self.geometry(f'{width}x{height}')

        self.loading_frame = ctk.CTkFrame(self)
        self.loading_frame.pack(fill='both', expand=True)
        ctk.CTkLabel(self.loading_frame, text='Loading...').pack(fill='both', expand=True)

        self.main_frame = ctk.CTkFrame(self)

        Thread(target=self.load).start()

        self.streaming_radio = None
        self.streaming = None

        self.mainloop()

    def stream(self, radio):
        if self.streaming is not None:
            self.streaming.kill()
            self.streaming = None
        if self.streaming_radio is not None:
            name = self.streaming_radio.name
            self.streaming_radio.stop()
            self.streaming_radio = None
            if name == radio.name:
                return

        for stream in radio.streams:
            try:
                self.streaming = Popen(['ffplay', '-autoexit', '-nodisp', stream['url']])
                self.streaming.wait(1)
            except TimeoutExpired:
                self.streaming_radio = radio
                radio.start(stream['type'])
                break

    def load(self):
        slider = ctk.CTkSlider(self.main_frame, from_=0, to=100, command=sliding, number_of_steps=100)
        slider.pack(fill='x', expand=True)
        slider.set(75)

        list_frame = ctk.CTkScrollableFrame(self.main_frame)
        list_frame.pack(fill='both', expand=True)
        list_frame.stream = self.stream

        html = get('https://radio.org.nz').text
        soup = BeautifulSoup(html, features='html.parser')
        for a in soup.find('ul', {'id': 'dynamic-radio-stations'}).find_all('a'):
            name = re.findall(r'radio\.org\.nz/(.*)/', a['href'])[0]
            Radio(list_frame, name).pack(fill='x', expand=True)
        self.main_frame.pack(fill='both', expand=True)
        self.loading_frame.pack_forget()


ctk.set_appearance_mode('Dark')
width, height = 480, 320


def sliding(value):
    run(['amixer', '-c', '2', 'set', 'Playback', f'{int(value)}%'], stdout=PIPE, stderr=STDOUT)


Main()
